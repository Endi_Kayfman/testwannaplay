using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]

public class DragAndShoot : MonoBehaviour
{
    [SerializeField] private float _forceMultiplier = 3f;

    private Vector3 _mousePressDownPos = Vector3.zero;
    private Vector3 _mouseReleasePos = Vector3.zero;

    private Rigidbody _rigidbody = null;

    public bool IsShoot = false;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }
    
    private void OnMouseDown()
    {
        _mousePressDownPos = Input.mousePosition;
    }
    
    private void OnMouseDrag()
    {
        Vector3 forceInit = (Input.mousePosition - _mousePressDownPos);
        Vector3 forceV = (new Vector3(forceInit.x, forceInit.y, forceInit.y)) * _forceMultiplier;

        if (!IsShoot)
        {
            DrawTrajectory.Instance.UpdateTrajectory(forceV, _rigidbody, transform.position);
        }
    }

    private void OnMouseUp()
    {
        DrawTrajectory.Instance.HideLine();
        _mouseReleasePos = Input.mousePosition;
        Shoot(_mouseReleasePos - _mousePressDownPos);
    }
    
    private void Shoot(Vector3 Force)
    {
        if(IsShoot)
        {
            return;
        }
        
        _rigidbody.AddForce(new Vector3(Force.x, Force.y, Force.y) * _forceMultiplier);
        IsShoot = true;
    }
}
