using System;
using System.Collections.Generic;
using UnityEngine;

public class TimeRewindBullet : TimeRewind
{
    public static event Action Collapsed = delegate { }; 

    private DragAndShoot _dragAndShootScript = null;

    protected override void TimeRewindController_ObjectStopped()
    {
        RewindTime();
    }

    protected override void Start()
    {
        _positionList = new List<Vector3>();
        _rigidbody = GetComponent<Rigidbody>();

        _dragAndShootScript = GetComponent<DragAndShoot>();
    }

    private void Update()
    {
        if (transform.position.y < -20f)
        {
            Collapsed();
        }
    }

    protected override void CanShoot()
    {
        _dragAndShootScript.IsShoot = false;
    }

    protected void OnCollisionEnter(Collision other)
    {
        if (_dragAndShootScript.IsShoot)
        {
            if(other.gameObject.TryGetComponent(out Collider collider))
            {
                Collapsed();
            }
        }
    }
}
