using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeRewindController : MonoBehaviour
{
    public static event Action ObjectStopped = delegate { }; 

    private List<Rigidbody> _newCubesRigidbodies = new List<Rigidbody>();

    private Coroutine _checkVelocityCor = null;
    
    private void OnEnable()
    {
        TimeRewindFigure.Spawned += TimeRewindFigure_Spawned;
        TimeRewindBullet.Collapsed += TimeRewindBullet_Collapsed;
    }
    
    private void OnDisable()
    {
        TimeRewindFigure.Spawned -= TimeRewindFigure_Spawned;
        TimeRewindBullet.Collapsed -= TimeRewindBullet_Collapsed;
    }

    private void TimeRewindFigure_Spawned(Rigidbody obj)
    {
        _newCubesRigidbodies.Add(obj);
    }
    
    private void TimeRewindBullet_Collapsed()
    {
        if (_checkVelocityCor == null)
        {
            _checkVelocityCor = StartCoroutine(CheckVelocityCor());
        }
    }
    
    private IEnumerator CheckVelocityCor()
    {
        var waiter = new WaitForSeconds(0.4f);

        bool corountineStopped = false;
        
        while (!corountineStopped)
        {
            Debug.Log(corountineStopped);
            
            for (int i = 0; i < _newCubesRigidbodies.Count; i++)
            {

                if (!_newCubesRigidbodies[i].velocity.magnitude.AlmostEquals(0f))
                {
                    break;
                }
                else
                {
                    if (i == _newCubesRigidbodies.Count - 1)
                    {
                        ObjectStopped();
                        corountineStopped = true;
                        break;
                    }
                }
            }

            yield return waiter;
        }

        _checkVelocityCor = null;
    }
}


