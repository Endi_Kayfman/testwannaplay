using System;
using System.Collections.Generic;
using UnityEngine;

public class TimeRewindFigure : TimeRewind
{
    public static event Action<Rigidbody> Spawned = delegate {  };

    protected override void TimeRewindController_ObjectStopped()
    {
        RewindTime();
    }

    protected override void Start()
    {
        _positionList = new List<Vector3>();
        _rigidbody = GetComponent<Rigidbody>();
        
        Spawned(_rigidbody);
    }

    protected override void CanShoot()
    {
        
    }
}
