using System.Collections.Generic;
using UnityEngine;

public class DrawTrajectory : MonoBehaviour
{
    [SerializeField] private LineRenderer _lineRenderer = null;
    [SerializeField] [Range(3, 30)] private int _lineSegmentCount = 20;
    
    [SerializeField] private GameObject _aimPoint;

    [SerializeField] private Material _lineMaterial = null;
    [SerializeField] private Material _transparentLineMaterial = null;
    
    private List<Vector3> _linePoints = new List<Vector3>();

    #region Singleton

    public static DrawTrajectory Instance;

    private void Awake()
    {
        Instance = this;
    }

    #endregion

    public void UpdateTrajectory(Vector3 forceVector, Rigidbody rigidbody, Vector3 startingPoint)
    {
        Vector3 velocity = (forceVector / rigidbody.mass) * Time.fixedDeltaTime;

        float flightDuration = (2 * velocity.y) / Physics.gravity.y;

        float stepTime = flightDuration / _lineSegmentCount;

        _linePoints.Clear();
        _linePoints.Add(startingPoint);

        _aimPoint.SetActive(true);
        
        for (int i = 1; i < _lineSegmentCount; i++)
        {
            float stepTimePassed = stepTime * i;

            Vector3 MovementVector = new Vector3(
                velocity.x * stepTimePassed,
                velocity.y * stepTimePassed - 0.5f * Physics.gravity.y * stepTimePassed * stepTimePassed,
                velocity.z * stepTimePassed
            );

            Vector3 newPointOnLine = -MovementVector + startingPoint;

            RaycastHit hit;
            if (Physics.Raycast(_linePoints[i - 1], newPointOnLine - _linePoints[i - 1], out hit,
                (newPointOnLine - _linePoints[i - 1]).magnitude))
            {
                _linePoints.Add(hit.point);
                _lineRenderer.material = _transparentLineMaterial;
                break;
            }
            else
            {
                _lineRenderer.material = _lineMaterial;
            }

            _linePoints.Add(newPointOnLine);
        }

        _lineRenderer.positionCount = _linePoints.Count;
        _lineRenderer.SetPositions(_linePoints.ToArray());
        _aimPoint.transform.position = _linePoints[_linePoints.Count - 2];
    }

    public void HideLine()
    {
        _lineRenderer.positionCount = 0;
        _aimPoint.SetActive(false);
    }
}