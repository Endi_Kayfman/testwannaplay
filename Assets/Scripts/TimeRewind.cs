using System.Collections.Generic;
using UnityEngine;

public abstract class TimeRewind : MonoBehaviour
{
    public bool _isRewind = false;
    
    protected List<Vector3> _positionList = new List<Vector3>();
    protected List<Quaternion> _quaternionsList = new List<Quaternion>();
    protected Rigidbody _rigidbody = null;

    private void OnEnable()
    {
        TimeRewindController.ObjectStopped += TimeRewindController_ObjectStopped;
    }

    private void OnDisable()
    {
        TimeRewindController.ObjectStopped -= TimeRewindController_ObjectStopped;
    }

    protected abstract void TimeRewindController_ObjectStopped();

    protected abstract void Start();

    private void FixedUpdate()
    {
        if (_isRewind)
        {
            if (_positionList.Count > 0)
            {
                int lastPosistion = _positionList.Count - 1;
                transform.position = _positionList[lastPosistion];
                _positionList.RemoveAt(lastPosistion);

                _rigidbody.isKinematic = true;
            }
            
            if (_quaternionsList.Count > 0)
            {
                int lastQuaternion = _quaternionsList.Count - 1;
                transform.rotation = _quaternionsList[lastQuaternion];
                _quaternionsList.RemoveAt(lastQuaternion);
            }
        }
        else
        {
            _positionList.Add(transform.position);
            _quaternionsList.Add(transform.rotation);
            
            _rigidbody.isKinematic = false;
        }

        if (_positionList.Count == 0)
        {
            _isRewind = !_isRewind;
            CanShoot();
        }
    }

    public void RewindTime()
    {
        _isRewind = !_isRewind;
    }

    protected abstract void CanShoot();
}
